# django-stub

This project assumes the use of pyenv + pipenv. See here for a [good introduction to these tools](https://hackernoon.com/reaching-python-development-nirvana-bb5692adf30c)
and how to install them.

## Installation of dependencies

1.) `pyenv install 3.7.3`

2.) cd to where you want this project to live, then `git clone https://gitlab.com/dnk8n/django-stub.git`

   Alternatively you can fork the project, and clone your fork. Prefereably clone with ssh if you have ssh keys set up

## Execute all the remaining commands from project folder, `cd django-stub`

## Installation of project

1.) If pipenv is not already installed, `pip install -U pipenv`

2.) `pipenv install`

## Configure local settings (one time operation)

1.) `pipenv run gen_local_settings`

## Run Dev Django server

1.) `pipenv run migrations`

2.) `pipenv run createsuperuser`

3.) `pipenv run server`

## To reset the environment

1.) pipenv run rm_sqlite
