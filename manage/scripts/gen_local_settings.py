#!/usr/bin/env python
"""A helper script which creates a starter local_settings.py"""

from django.utils.crypto import get_random_string
from pathlib import Path

chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
secret_key = get_random_string(50, chars)

template_local_settings = f"""
from django_stub.settings import *
SECRET_KEY = '{secret_key}'
"""
with Path('src', 'django_stub', 'local_settings.py').open('w') as local_settings:
    local_settings.write(template_local_settings)
